<?php
require_once '../include/formvalidator.php';
require_once '../include/fg_membersite.php';
require_once 'PHPUnit/Autoload.php';

	class SignUp_Test extends PHPUnit_Framework_TestCase
	{
		//var $frmvalidator = null;

		function test_ConnectDB()
	{
		$servername = "localhost";
		$username = "root";
		$password = "TwentyFour7";
		$dbName = "TwentyFour7";
    	$conn = mysql_connect($servername,$username,$password);
    	$db = mysql_select_db("TwentyFour7",$conn);
    	if(!$conn){
    		$this->assertFalse(False);
    	}
    	else
    		$this->assertTrue(True);

	}
		function testSetUsername(){
			$member = new FGMembersite();
			$username = $member->setUsername('TwentyFour7');
			$this->assertEquals('TwentyFour7',$member->username);
		}

		function testGetUsername(){
			$member = new FGMembersite();
			$username = $member->setUsername('TwentyFour7');
			$this->assertEquals($member->username,$member->getUsername());
		}

		function testInitDB(){
			$member = new FGMembersite();
			$member->InitDB('localhost','root','TwentyFour7','TwentyFour7','product_account');
			$this->assertEquals($member->db_host,'localhost');
			$this->assertEquals($member->username,'root');
			$this->assertEquals($member->pwd,'TwentyFour7');
			$this->assertEquals($member->database,'TwentyFour7');
			$this->assertEquals($member->tablename,'product_account');
		}

		function testSetAdminEmail(){
			$member = new FGMembersite();
			$member->SetAdminEmail('gzhewei2@gmail.com');
			$this->assertEquals('gzhewei2@gmail.com',$member->admin_email);
		}

		function testSetWebsiteName(){
			$member = new FGMembersite();
			$member->SetWebsiteName('TwentyFour7');
			$this->assertEquals('TwentyFour7',$member->sitename);
		}

		function testSetRandomKey(){
			$member = new FGMembersite();
			$member->SetRandomKey('random');
			$this->assertEquals('random',$member->rand_key);
		}

		function testRegisterUser(){
			$member = new FGMembersite();
			$this->assertFalse($member->RegisterUser());
		}

		function testConfirmUser(){
			$member = new FGMembersite();
			$this->assertFalse($member->ConfirmUser());
		}
}

?>