<?php
require_once "PHPUnit/Autoload.php";

	class DataTest extends PHPUnit_Framework_TestCase
	{
		function testGetProductName(){
			$productDAO = new productDAO();
			$product = $productDAO->getProduct(1,False);
			$this->assertEquals(
				array(
					array("id" => 1, "name" => "Service 1"),
					array("id" => 2, "name" => "Service 2")),
				  $product);
			
		}
	} 

?>